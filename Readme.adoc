= Numeric overflow

//tag::abstract[]

Numeric overflow vulnerability appears when the range of
numeric value is not being checked.

//end::abstract[]

For example, 
Integer in C# is commonly 32 bits, therefore it ranges
between 2,147,483,647 to -2,147,483,648.

If a given value is bigger or smaller than Integer
range, the variable overflows or wraps around. It turns to a negative
or positive numbers respectively.

This vulnerability can cause logic bypass often
result in serious impact. Think how in a banking
application a daily transaction threshold can get
passed by using this vulnerability?

This vulnerabilities affects all numeric types
that do no use dynamic memory allocation.

//tag::lab[]

== Lab

Your objective in each lab is to pass the build.

=== Task 0

*Fork* and clone this repository.
Install `docker` and `make` on your system.

. Build the program: `make build`.
. Run it: `make run`.
. Run unit tests: `make test`.
. Run security tests: `make securitytest`.

[NOTE]
--
The last test will fail.
--

=== Task 1

Run the project and give it an amount.
Review the program code try to find out
why security tests fails.

[IMPORTANT]
--
Avoid looking at tests or patch file and try to
spot the security bug on your own.
--

=== Task 2

The program is vulnerable to Numeric Overflow.
Perform both run-time and static analysis to test for this vulnerability.
Find out how to patch this vulnerability.

=== Task 3

Review `test/programSecurityTest.cs` and see how security tests
works. Review your patch from the previous task.
Make sure this time the security tests pass.
If you stuck, move to the next task.

=== Task 4

Check out the `patch` branch:

* Read link:cheatsheet.adoc[] and apply to your solution.
* Review the patched program.
* Run all tests.

=== Task 5

Push you code and make sure build is passed
Finally, send a pull request (PR).

//end::lab[]

//tag::references[]

== References

* OWASP Application Security Verification Standard 4.0, 5.4.3
* CWE 190
* https://wiki.sei.cmu.edu/confluence/display/java/NUM00-J.+Detect+or+prevent+integer+overflow[NUM00-J. Detect or prevent integer overflow]

//end::references[]

include::CONTRIBUTING.adoc[]

== License

See link:LICENSE[]
